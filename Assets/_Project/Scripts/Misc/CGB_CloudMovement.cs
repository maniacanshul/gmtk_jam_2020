﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CGB_CloudMovement : MonoBehaviour
{
    private Vector3 __initialPosition;
    private Vector3 __goToPos;

    private float __tweenTime;

    private void Awake()
    {
        __initialPosition = transform.localPosition;
        __goToPos = __initialPosition;
        __goToPos.x += Random.Range(-50, 50);
        __goToPos.z += Random.Range(-50, 50);
        __tweenTime = Random.Range(10, 20);
    }

    private void Start()
    {
        MoveToGoto();
    }

    private void MoveToGoto()
    {
        transform.DOKill();
        transform.DOLocalMove(__goToPos, __tweenTime).OnComplete(MoveToOriginal);
    }

    private void MoveToOriginal()
    {
        transform.DOKill();
        transform.DOLocalMove(__initialPosition, __tweenTime).OnComplete(MoveToGoto);
    }
}
