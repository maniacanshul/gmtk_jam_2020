﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CGB_GameManager : MonoBehaviour
{
    public CGB_PlayerManager player1Manager;
    public CGB_PlayerManager player2Manager;
    public CGB_CollectiblesManager collectiblesManager;
    public CGB_EnemyManager enemyManager;
    public CGB_HUDManager hUDManager;
    public CGB_MainMenuManager mainMenuManager;

    private bool __secondPlayer;

    public void StartGame(bool secondPlayer)
    {
        enemyManager.SetupEnemySpawners();
        collectiblesManager.SetupLevel();
        player1Manager.SetupPlayer(this);

        __secondPlayer = secondPlayer;
        if (secondPlayer)
        {
            player2Manager.SetupPlayer(this);
        }

        enemyManager.StartEnemySpawning();
        hUDManager.gameObject.SetActive(true);
    }

    public void GameWon(string tag)
    {
        hUDManager.gameObject.SetActive(false);
        mainMenuManager.GameOver(tag == "Player1", __secondPlayer);

        player1Manager.RemoveSubscriptions();
        player2Manager.RemoveSubscriptions();
    }
}
