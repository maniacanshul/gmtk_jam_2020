﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGB_CollectibleObject : MonoBehaviour
{
    [SerializeField]
    private Animator _animator;

    private CGB_CollectiblesManager __collectibleManager;

    private bool __canBeCollected;

    public void SetupReference(CGB_CollectiblesManager manager)
    {
        __canBeCollected = true;
        __collectibleManager = manager;
    }

    private void OnTriggerEnter(Collider hit)
    {
        if (!__canBeCollected)
        {
            return;
        }

        if (hit.CompareTag("Player1"))
        {
            __canBeCollected = false;
            _animator.Play("Collected");
            __collectibleManager.CollectibleObjectPlayerCollisionCallback(true);
        }

        if (hit.CompareTag("Player2"))
        {
            __canBeCollected = false;
            _animator.Play("Collected");
            __collectibleManager.CollectibleObjectPlayerCollisionCallback(false);
        }
    }

    public void CollectedAnimationOverCallback()
    {
        gameObject.SetActive(false);
    }
}
