﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGB_CollectiblesManager : MonoBehaviour
{
    [SerializeField]
    private CGB_PlayerManager _player1, _player2;
    [SerializeField]
    private CGB_HUDManager _hudManager;

    [SerializeField]
    private GameObject _finishLine;

    [SerializeField]
    private Transform[] _spawnPoints;

    [SerializeField]
    private CGB_CollectibleObject[] _collectibles;

    [Header("Level Details")]
    public int numberOfCollectibles;

    [HideInInspector]
    public int maxCollectiblePerPerson;

    private int __collectiblesCollectedPlayer1;
    private int __collectiblesCollectedPlayer2;

    public void SetupLevel()
    {
        __collectiblesCollectedPlayer1 = 0;
        __collectiblesCollectedPlayer2 = 0;
        _finishLine.SetActive(false);

        _hudManager.SetCollectiblesCountPlayer1(__collectiblesCollectedPlayer1, maxCollectiblePerPerson);
        _hudManager.SetCollectiblesCountPlayer2(__collectiblesCollectedPlayer2, maxCollectiblePerPerson);
        ShuffleSpawnPoint();
        SpawnCollectibles();
    }

    private void ShuffleSpawnPoint()
    {
        for (int i = 0; i < _spawnPoints.Length; i++)
        {
            int j = Random.Range(0, i);
            Transform temp = _spawnPoints[i];
            _spawnPoints[i] = _spawnPoints[j];
            _spawnPoints[j] = temp;
        }
    }

    private void SpawnCollectibles()
    {
        for (int i = 0; i < _collectibles.Length; i++)
        {
            _collectibles[i].gameObject.SetActive(false);
            if (i < numberOfCollectibles)
            {
                _collectibles[i].gameObject.SetActive(true);
                _collectibles[i].SetupReference(this);
                _collectibles[i].transform.position = _spawnPoints[i].position;
            }
        }
    }

    public void CollectibleObjectPlayerCollisionCallback(bool player1)
    {
        if (player1)
        {
            __collectiblesCollectedPlayer1++;
            _hudManager.SetCollectiblesCountPlayer1(__collectiblesCollectedPlayer1, maxCollectiblePerPerson);
        }
        else
        {
            __collectiblesCollectedPlayer2++;
            _hudManager.SetCollectiblesCountPlayer2(__collectiblesCollectedPlayer2, maxCollectiblePerPerson);
        }

        if (__collectiblesCollectedPlayer1 >= maxCollectiblePerPerson || __collectiblesCollectedPlayer2 >= maxCollectiblePerPerson)
        {
            _finishLine.SetActive(true);
            CGB_AudioManager.instance.PlaySFX(SFX.FINISH_LINE_SPAWN);

            if (__collectiblesCollectedPlayer1 >= maxCollectiblePerPerson)
            {
                _player1.CanUseFinishLine();
            }
            else
            {
                _player2.CanUseFinishLine();
            }
        }
        else
        {
            CGB_AudioManager.instance.PlaySFX(SFX.MUSHROOM_COLLECTION);
        }
    }
}
