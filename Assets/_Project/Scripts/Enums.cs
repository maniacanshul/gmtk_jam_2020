﻿public enum KEYBOARD_KEYS
{
    W,
    S,
    A,
    D,
    UP,
    DOWN,
    LEFT,
    RIGHT
}

public enum CONTROLS_STATE
{
    NORMAL,
    RIGHT_SHIFT,
    REVERSE,
    LEFT_SHIFT
}

public enum ENEMY_TYPE
{
    Clockwise,
    AntiClockwise
}

public enum SFX
{
    MUSHROOM_COLLECTION,
    ENEMY_HIT_CLOCKWISE,
    ENEMY_HIT_ANTICLOCKWISE,
    FINISH_LINE_SPAWN,
    FINISH_LINE
}