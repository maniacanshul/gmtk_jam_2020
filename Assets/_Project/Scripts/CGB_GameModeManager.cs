﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGB_GameModeManager : MonoBehaviour
{
    public Camera player1Camera;
    public CGB_PlayerManager player2;
    public CGB_HUDManager hUDManager;
    public CGB_CollectiblesManager collectiblesManager;
    public CGB_GameManager gameManager;

    private bool __splitScreenMode;

    public void SetupSinglePlayMode()
    {
        player2.gameObject.SetActive(false);
        collectiblesManager.numberOfCollectibles = 5;
        collectiblesManager.maxCollectiblePerPerson = 5;
        player1Camera.rect = new Rect(0, 0, 1, 1);
        hUDManager.SetupHUDGameMode(true);
        __splitScreenMode = false;

        Invoke("GameStartDelay", 1);
    }

    public void SetupSplitscreenPlay()
    {
        player2.gameObject.SetActive(true);
        collectiblesManager.numberOfCollectibles = 9;
        collectiblesManager.maxCollectiblePerPerson = 5;
        player1Camera.rect = new Rect(0, 0.5f, 1, 0.5f);
        hUDManager.SetupHUDGameMode(false);
        __splitScreenMode = true;

        Invoke("GameStartDelay", 1);
    }

    private void GameStartDelay()
    {
        gameManager.StartGame(__splitScreenMode);
    }
}
