﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CGB_PlayerMovementManager : MonoBehaviour
{
    [SerializeField]
    private Animator _animator;

    [SerializeField]
    private float _movementSpeed;

    [SerializeField]
    private float _rotationSpeed;

    private bool __canMove;
    private float __initialYPos;

    private float __animationSpeed;

    private void Awake()
    {
        __initialYPos = transform.localPosition.y;
        __canMove = true;
    }

    public void MovePlayer(Vector3 movementDirection)
    {
        if (!__canMove)
        {
            return;
        }

        __animationSpeed = 0.25f;
        _animator.SetFloat("Speed", 2);

        Vector3 tempPos = transform.localPosition;
        tempPos += movementDirection.z * Time.deltaTime * _movementSpeed * transform.forward;
        transform.localPosition = tempPos;

        Vector3 tempRot = transform.localEulerAngles;
        tempRot.y += movementDirection.x * Time.deltaTime * _rotationSpeed;
        transform.localEulerAngles = tempRot;
    }

    private void Update()
    {
        if (__animationSpeed > 0)
        {
            __animationSpeed -= Time.deltaTime;
            if (__animationSpeed <= 0)
            {
                _animator.SetFloat("Speed", 0);
            }
        }
    }

    public void ControlSwitchJumpEvent()
    {
        _animator.Play("Jump");

        __canMove = false;

        transform.DOKill();
        transform.DOLocalMoveY(__initialYPos + 3, 0.5f);
        transform.DOLocalMoveY(__initialYPos, 0.5f).SetDelay(4).OnComplete(() =>
       {
           __canMove = true;
       });

        transform.DOLocalMoveX(transform.localPosition.x + Random.Range(2, 4), 0.15f);
        transform.DOLocalMoveZ(transform.localPosition.z + Random.Range(2, 4), 0.15f);
    }
}
