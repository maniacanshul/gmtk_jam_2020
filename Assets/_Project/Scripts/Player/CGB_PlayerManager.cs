﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGB_PlayerManager : MonoBehaviour
{
    [SerializeField]
    private CGB_PlayerControlSchemeDisplay _controlSchemeDisplay;
    [SerializeField]
    private CGB_PlayerMovementManager _movementManager;
    [SerializeField]
    private CGB_CameraFollow cameraShake;

    [Header("Keys")]
    [SerializeField]
    private KEYBOARD_KEYS _forwardKey;
    [SerializeField]
    private KEYBOARD_KEYS _backwardKey;
    [SerializeField]
    private KEYBOARD_KEYS _leftKey;
    [SerializeField]
    private KEYBOARD_KEYS _rightKey;

    private CONTROLS_STATE __currentControlState;
    private CGB_GameManager __gameManager;

    private bool __canUseFinishLine;

    #region  Subscriptions
    private void OnEnable()
    {
        SetupSubscriptions();
    }

    private void OnDisable()
    {
        RemoveSubscriptions();
    }

    private void SetupSubscriptions()
    {
        CGB_InputKeyReader.OnKeyPressed -= KeyPressedEventSubscription;
        CGB_InputKeyReader.OnKeyPressed += KeyPressedEventSubscription;
    }

    public void RemoveSubscriptions()
    {
        CGB_InputKeyReader.OnKeyPressed -= KeyPressedEventSubscription;
    }
    #endregion

    public void SetupPlayer(CGB_GameManager manager)
    {
        __canUseFinishLine = false;
        __gameManager = manager;
        __currentControlState = CONTROLS_STATE.NORMAL;
        _controlSchemeDisplay.ShowControlScheme();
        _controlSchemeDisplay.UpdateControlScheme(__currentControlState, true, false);
    }

    public void UpdateControlScheme(bool clockwise)
    {
        switch (__currentControlState)
        {
            case CONTROLS_STATE.NORMAL:
                {
                    __currentControlState = clockwise ? CONTROLS_STATE.RIGHT_SHIFT : CONTROLS_STATE.LEFT_SHIFT;
                    break;
                }
            case CONTROLS_STATE.RIGHT_SHIFT:
                {
                    __currentControlState = clockwise ? CONTROLS_STATE.REVERSE : CONTROLS_STATE.NORMAL;
                    break;
                }
            case CONTROLS_STATE.REVERSE:
                {
                    __currentControlState = clockwise ? CONTROLS_STATE.LEFT_SHIFT : CONTROLS_STATE.RIGHT_SHIFT;
                    break;
                }
            case CONTROLS_STATE.LEFT_SHIFT:
                {
                    __currentControlState = clockwise ? CONTROLS_STATE.NORMAL : CONTROLS_STATE.REVERSE;
                    break;
                }
        }

        _controlSchemeDisplay.ShowControlScheme();
        _controlSchemeDisplay.UpdateControlScheme(__currentControlState, clockwise, true);
        _movementManager.ControlSwitchJumpEvent();
        CameraShakeOnControlChange();
    }

    private void CameraShakeOnControlChange()
    {
        cameraShake.shakeDuration = 0.25f;
    }

    private void KeyPressedEventSubscription(KEYBOARD_KEYS key)
    {
        Vector3 movementVector = Vector3.zero;

        switch (__currentControlState)
        {
            case CONTROLS_STATE.NORMAL:
                {
                    NormalControls(key, ref movementVector);
                    break;
                }

            case CONTROLS_STATE.LEFT_SHIFT:
                {
                    LeftShiftControls(key, ref movementVector);
                    break;
                }

            case CONTROLS_STATE.REVERSE:
                {
                    ReverseControls(key, ref movementVector);
                    break;
                }

            case CONTROLS_STATE.RIGHT_SHIFT:
                {
                    RightShiftControls(key, ref movementVector);
                    break;
                }
        }

        if (movementVector.z != 0 || movementVector.x != 0)
        {
            _movementManager.MovePlayer(movementVector);
        }
    }

    #region Controls Mapping
    private void NormalControls(KEYBOARD_KEYS keyPressed, ref Vector3 movementVector)
    {
        if (keyPressed == _forwardKey)
        {
            movementVector.z = 1;
        }
        else if (keyPressed == _backwardKey)
        {
            movementVector.z = -1;
        }
        else if (keyPressed == _leftKey)
        {
            movementVector.x = -1;
        }
        else if (keyPressed == _rightKey)
        {
            movementVector.x = 1;
        }
    }

    private void ReverseControls(KEYBOARD_KEYS keyPressed, ref Vector3 movementVector)
    {
        if (keyPressed == _forwardKey)
        {
            movementVector.z = -1;
        }
        else if (keyPressed == _backwardKey)
        {
            movementVector.z = 1;
        }
        else if (keyPressed == _leftKey)
        {
            movementVector.x = 1;
        }
        else if (keyPressed == _rightKey)
        {
            movementVector.x = -1;
        }
    }

    private void LeftShiftControls(KEYBOARD_KEYS keyPressed, ref Vector3 movementVector)
    {
        if (keyPressed == _forwardKey)
        {
            movementVector.x = -1;
        }
        else if (keyPressed == _backwardKey)
        {
            movementVector.x = 1;
        }
        else if (keyPressed == _leftKey)
        {
            movementVector.z = -1;
        }
        else if (keyPressed == _rightKey)
        {
            movementVector.z = 1;
        }
    }

    private void RightShiftControls(KEYBOARD_KEYS keyPressed, ref Vector3 movementVector)
    {
        if (keyPressed == _forwardKey)
        {
            movementVector.x = 1;
        }
        else if (keyPressed == _backwardKey)
        {
            movementVector.x = -1;
        }
        else if (keyPressed == _leftKey)
        {
            movementVector.z = 1;
        }
        else if (keyPressed == _rightKey)
        {
            movementVector.z = -1;
        }
    }
    #endregion

    public void CanUseFinishLine()
    {
        __canUseFinishLine = true;
    }

    public void GameWonCollisionCallback()
    {
        if (__canUseFinishLine)
        {
            __gameManager.GameWon(transform.tag);
        }
    }
}
