﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CGB_PlayerControlSchemeDisplay : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer _w, _s, _a, _d, _rotationClockwise, _rotationAntiClockwise;

    private Vector3 __topPos, __bottomPos, __leftPos, __rightPos;

    private void Awake()
    {
        __topPos = _w.transform.localPosition;
        __bottomPos = _s.transform.localPosition;
        __leftPos = _a.transform.localPosition;
        __rightPos = _d.transform.localPosition;
    }

    public void ShowControlScheme()
    {
        _w.transform.DOKill();
        _s.transform.DOKill();
        _a.transform.DOKill();
        _d.transform.DOKill();

        float maxFade = 0.75f;
        float fadeTime = 0.25f;
        float stayTime = 3;

        _w.DOFade(maxFade, fadeTime);
        _w.DOFade(0.15f, fadeTime).SetDelay(fadeTime + stayTime);


        _s.DOFade(maxFade, fadeTime);
        _s.DOFade(0.15f, fadeTime).SetDelay(fadeTime + stayTime);


        _a.DOFade(maxFade, fadeTime);
        _a.DOFade(0.15f, fadeTime).SetDelay(fadeTime + stayTime);


        _d.DOFade(maxFade, fadeTime);
        _d.DOFade(0.15f, fadeTime).SetDelay(fadeTime + stayTime);
    }

    public void UpdateControlScheme(CONTROLS_STATE state, bool clockwise, bool showDirectionImage)
    {
        float movementTweenTime = 0.5f;
        float delay = 0.25f;
        switch (state)
        {
            case CONTROLS_STATE.NORMAL:
                {
                    _w.transform.DOLocalMove(__topPos, movementTweenTime).SetDelay(delay);
                    _s.transform.DOLocalMove(__bottomPos, movementTweenTime).SetDelay(delay);

                    _a.transform.DOLocalMove(__leftPos, movementTweenTime).SetDelay(delay);
                    _d.transform.DOLocalMove(__rightPos, movementTweenTime).SetDelay(delay);
                    break;
                }
            case CONTROLS_STATE.REVERSE:
                {
                    _s.transform.DOLocalMove(__topPos, movementTweenTime).SetDelay(delay);
                    _w.transform.DOLocalMove(__bottomPos, movementTweenTime).SetDelay(delay);

                    _d.transform.DOLocalMove(__leftPos, movementTweenTime).SetDelay(delay);
                    _a.transform.DOLocalMove(__rightPos, movementTweenTime).SetDelay(delay);
                    break;
                }
            case CONTROLS_STATE.LEFT_SHIFT:
                {
                    _w.transform.DOLocalMove(__leftPos, movementTweenTime).SetDelay(delay);
                    _s.transform.DOLocalMove(__rightPos, movementTweenTime).SetDelay(delay);

                    _d.transform.DOLocalMove(__topPos, movementTweenTime).SetDelay(delay);
                    _a.transform.DOLocalMove(__bottomPos, movementTweenTime).SetDelay(delay);
                    break;
                }
            case CONTROLS_STATE.RIGHT_SHIFT:
                {
                    _s.transform.DOLocalMove(__leftPos, movementTweenTime).SetDelay(delay);
                    _w.transform.DOLocalMove(__rightPos, movementTweenTime).SetDelay(delay);

                    _a.transform.DOLocalMove(__topPos, movementTweenTime).SetDelay(delay);
                    _d.transform.DOLocalMove(__bottomPos, movementTweenTime).SetDelay(delay);
                    break;
                }
        }

        if (showDirectionImage)
        {
            if (clockwise)
            {
                _rotationClockwise.DOFade(0, 0);
                _rotationClockwise.DOFade(1, movementTweenTime);
                _rotationClockwise.DOFade(0, movementTweenTime).SetDelay(delay * 4);
            }
            else
            {
                _rotationClockwise.DOFade(0, 0);
                _rotationClockwise.DOFade(1, movementTweenTime);
                _rotationClockwise.DOFade(0, movementTweenTime).SetDelay(delay * 4);
            }
        }
    }
}
