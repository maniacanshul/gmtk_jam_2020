﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGB_PlayerCollisionDetection : MonoBehaviour
{
    [SerializeField]
    private CGB_PlayerManager _playerManager;

    private void OnTriggerEnter(Collider hit)
    {
        if (hit.CompareTag("Clockwise"))
        {
            CGB_AudioManager.instance.PlaySFX(SFX.ENEMY_HIT_CLOCKWISE);
            _playerManager.UpdateControlScheme(true);
        }
        else if (hit.CompareTag("AntiClockwise"))
        {
            CGB_AudioManager.instance.PlaySFX(SFX.ENEMY_HIT_ANTICLOCKWISE);
            _playerManager.UpdateControlScheme(false);
        }
        else if (hit.CompareTag("FinishLine"))
        {
            CGB_AudioManager.instance.PlaySFX(SFX.FINISH_LINE);
            _playerManager.GameWonCollisionCallback();
        }
    }
}
