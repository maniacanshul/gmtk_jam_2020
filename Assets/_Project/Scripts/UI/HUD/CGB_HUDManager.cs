﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CGB_HUDManager : MonoBehaviour
{
    [SerializeField]
    private Text _collectiblesCountPlayer1Text;

    [SerializeField]
    private Text _collectiblesCountPlayer2Text;

    [SerializeField]
    private GameObject _player2MushroomImage;

    public void SetCollectiblesCountPlayer1(int current, int total)
    {
        _collectiblesCountPlayer1Text.text = current + " / " + total;

        if (current == total)
        {
            _collectiblesCountPlayer1Text.transform.DOPunchScale(Vector3.one * 0.1f, 0.5f).SetLoops(-1);
        }
        else
        {
            _collectiblesCountPlayer1Text.transform.DOPunchScale(Vector3.one * 1, 0.25f);
        }
    }

    public void SetCollectiblesCountPlayer2(int current, int total)
    {
        _collectiblesCountPlayer2Text.text = current + " / " + total;

        if (current == total)
        {
            _collectiblesCountPlayer2Text.transform.DOPunchScale(Vector3.one * 0.1f, 0.5f).SetLoops(-1);
        }
        else
        {
            _collectiblesCountPlayer2Text.transform.DOPunchScale(Vector3.one * 1, 0.25f);
        }
    }

    public void SetupHUDGameMode(bool singlePlayer)
    {
        _player2MushroomImage.SetActive(!singlePlayer);
        _collectiblesCountPlayer2Text.gameObject.SetActive(!singlePlayer);
    }
}
