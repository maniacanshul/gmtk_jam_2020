﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CGB_MainMenuManager : MonoBehaviour
{
    [SerializeField]
    private Image _player1CharMask, _player2CharMask;

    [SerializeField]
    private Camera menuCamera;

    [SerializeField]
    private CanvasGroup canvasGroupMenu, canvasGroupOverlay, _gameWonPanel;

    [SerializeField]
    private Text _resultText;

    public void SplitScreenButtonOnHover(bool isHovering)
    {
        _player2CharMask.DOKill();
        if (isHovering)
        {
            _player2CharMask.DOFade(0, 0.25f);
        }
        else
        {
            _player2CharMask.DOFade(1, 0.25f);
        }
    }

    public void HideMenu()
    {
        menuCamera.DOFieldOfView(0, 0.5f).OnComplete(() =>
        {
            menuCamera.enabled = false;
        });
        canvasGroupOverlay.DOFade(0, 0.5f);
        canvasGroupMenu.DOFade(0, 0.5f);
        canvasGroupMenu.interactable = false;
        canvasGroupMenu.blocksRaycasts = false;

        _gameWonPanel.DOFade(0, 0f);
        _gameWonPanel.blocksRaycasts = false;
        _gameWonPanel.interactable = false;
    }

    public void GameOver(bool didPlayer1Win, bool secondPlayer)
    {
        _player2CharMask.DOFade(didPlayer1Win ? 1 : 0, 0);
        _player1CharMask.DOFade(didPlayer1Win ? 0 : 1, 0);

        menuCamera.enabled = true;
        menuCamera.DOFieldOfView(60, 0.5f);

        _gameWonPanel.DOFade(1, 0.25f);
        _gameWonPanel.blocksRaycasts = true;
        _gameWonPanel.interactable = true;
        canvasGroupOverlay.DOFade(1, 0.25f);

        if (secondPlayer)
        {
            _resultText.text = didPlayer1Win ? "PLAYER 1 WON" : " PLAYER 2 WON";
        }
        else
        {
            _resultText.text = "YOU WON!";
        }
    }

    public void MainMenuButton()
    {
        SceneManager.LoadScene(0);
    }
}
