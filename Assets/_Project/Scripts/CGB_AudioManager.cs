﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGB_AudioManager : MonoBehaviour
{
    public static CGB_AudioManager instance;

    [SerializeField]
    private SFXData[] sfxData;
    [SerializeField]
    private List<AudioSource> sfxSources;

    private List<AudioSource> sfxSourcesBeingUsed;

    private void Awake()
    {
        instance = this;
        sfxSourcesBeingUsed = new List<AudioSource>();
    }

    //Pool all busy sources into free sources. 
    public void SetupSFXAudioSourcesForTheLevel()
    {
        sfxSources.AddRange(sfxSourcesBeingUsed);
        sfxSourcesBeingUsed.Clear();
    }

    //Callback from different systems to play sfx. 
    public void PlaySFX(SFX type)
    {
        for (int i = 0; i < sfxData.Length; i++)
        {
            if (sfxData[i].type == type)
            {
                PlaySFXClip(sfxData[i].clip);
                break;
            }
        }
    }

    //Find the free audio source and play the clip.
    private void PlaySFXClip(AudioClip clip)
    {
        DespawnAudioSource();
        if (sfxSources.Count > 0)
        {
            AudioSource tempSource = sfxSources[0];
            tempSource.clip = clip;
            tempSource.Play();

            sfxSources.Remove(tempSource);
            sfxSourcesBeingUsed.Add(tempSource);
        }
    }

    //Loop through all the busy audio sources and pool them back in if they are not being used currently.
    private void DespawnAudioSource()
    {
        for (int i = sfxSourcesBeingUsed.Count - 1; i >= 0; i--)
        {
            if (!sfxSourcesBeingUsed[i].isPlaying)
            {
                sfxSources.Add(sfxSourcesBeingUsed[i]);
                sfxSourcesBeingUsed.RemoveAt(i);
            }
        }
    }

    [System.Serializable]
    public struct SFXData
    {
        public SFX type;
        public AudioClip clip;
    }
}