﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGB_InputKeyReader : MonoBehaviour
{
    public delegate void KeyPressed(KEYBOARD_KEYS key);
    public static event KeyPressed OnKeyPressed;

    private KEYBOARD_KEYS __keyboardKeyPressed;

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            __keyboardKeyPressed = KEYBOARD_KEYS.W;
            FireKeyPressedEvent();
        }
        else if (Input.GetKey(KeyCode.S))
        {
            __keyboardKeyPressed = KEYBOARD_KEYS.S;
            FireKeyPressedEvent();
        }

        if (Input.GetKey(KeyCode.A))
        {
            __keyboardKeyPressed = KEYBOARD_KEYS.A;
            FireKeyPressedEvent();
        }
        else if (Input.GetKey(KeyCode.D))
        {
            __keyboardKeyPressed = KEYBOARD_KEYS.D;
            FireKeyPressedEvent();
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            __keyboardKeyPressed = KEYBOARD_KEYS.UP;
            FireKeyPressedEvent();
        }
        else if (Input.GetKey(KeyCode.DownArrow))
        {
            __keyboardKeyPressed = KEYBOARD_KEYS.DOWN;
            FireKeyPressedEvent();
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            __keyboardKeyPressed = KEYBOARD_KEYS.LEFT;
            FireKeyPressedEvent();
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            __keyboardKeyPressed = KEYBOARD_KEYS.RIGHT;
            FireKeyPressedEvent();
        }
    }

    private void FireKeyPressedEvent()
    {
        OnKeyPressed?.Invoke(__keyboardKeyPressed);
    }
}
