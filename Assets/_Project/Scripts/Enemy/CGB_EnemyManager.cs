﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGB_EnemyManager : MonoBehaviour
{
    [SerializeField]
    private CGB_EnemySpawner[] _spawners;

    public void SetupEnemySpawners()
    {
        for (int i = 0; i < _spawners.Length; i++)
        {
            _spawners[i].SetupEnemySpawner();
        }
    }

    public void StartEnemySpawning()
    {
        for (int i = 0; i < _spawners.Length; i++)
        {
            _spawners[i].StartEnemySpawn();
        }
    }

    public void StopEnemySpanwing()
    {
        for (int i = 0; i < _spawners.Length; i++)
        {
            _spawners[i].StopEnemySpawn();
        }
    }
}
