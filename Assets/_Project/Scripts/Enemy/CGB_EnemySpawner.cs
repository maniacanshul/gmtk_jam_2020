﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CGB_EnemySpawner : MonoBehaviour
{
    [SerializeField]
    private Transform _endPoint;

    [SerializeField]
    private List<CGB_EnemyObject> _enemies;

    [Header("Gameplay Values")]
    [SerializeField]
    private float _spawnInterval;

    [SerializeField]
    private float _randomSpawnInterval;

    [SerializeField]
    private float _tweenTime;

    [SerializeField]
    private ENEMY_TYPE type;

    private List<CGB_EnemyObject> __enemiesBeingUsed;
    private Coroutine __enemySpawnCoroutine;


    private void Awake()
    {
        __enemiesBeingUsed = new List<CGB_EnemyObject>();
    }

    public void SetupEnemySpawner()
    {
        _enemies.AddRange(__enemiesBeingUsed);
        __enemiesBeingUsed.Clear();

        StopEnemySpawn();
    }

    private void DisableAllEnemies()
    {
        for (int i = 0; i < _enemies.Count; i++)
        {
            _enemies[i].transform.DOKill();
            _enemies[i].gameObject.SetActive(false);
            _enemies[i].SetupEnemySpawnerReference(this, type);
        }
    }

    public void StartEnemySpawn()
    {
        if (__enemySpawnCoroutine != null)
        {
            StopCoroutine(__enemySpawnCoroutine);
        }

        __enemySpawnCoroutine = StartCoroutine(EnemySpawnerCoroutine());
    }

    public void StopEnemySpawn()
    {
        if (__enemySpawnCoroutine != null)
        {
            StopCoroutine(__enemySpawnCoroutine);
        }

        DisableAllEnemies();
    }

    private IEnumerator EnemySpawnerCoroutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(_spawnInterval + Random.Range(0, _randomSpawnInterval));
            SpawnEnemy();
            yield return new WaitForEndOfFrame();
        }
    }

    private void SpawnEnemy()
    {
        if (_enemies.Count > 0)
        {
            CGB_EnemyObject tempEnemy = _enemies[0];

            _enemies.Remove(tempEnemy);
            __enemiesBeingUsed.Add(tempEnemy);

            tempEnemy.transform.DOKill();
            tempEnemy.gameObject.SetActive(true);

            Vector3 goTo = tempEnemy.transform.position;
            goTo.x = transform.position.x;
            goTo.z = transform.position.z;
            tempEnemy.transform.position = goTo;

            goTo.z = _endPoint.position.z;
            goTo.x = _endPoint.position.x;
            tempEnemy.transform.DOMove(goTo, _tweenTime).SetEase(Ease.Linear).OnComplete(() =>
           {
               PoolEnemyCallback(null);
           });
        }
    }

    public void PoolEnemyCallback(CGB_EnemyObject tempEnemy = null)
    {
        if (tempEnemy == null)
        {
            PoolBackEnemy(__enemiesBeingUsed[0]);
        }
        else
        {
            PoolBackEnemy(tempEnemy);
        }
    }

    private void PoolBackEnemy(CGB_EnemyObject tempEnemy)
    {
        _enemies.Add(tempEnemy);
        __enemiesBeingUsed.Remove(tempEnemy);
        tempEnemy.transform.DOKill();
        tempEnemy.gameObject.SetActive(false);
    }
}
