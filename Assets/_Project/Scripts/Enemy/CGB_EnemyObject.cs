﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CGB_EnemyObject : MonoBehaviour
{
    [SerializeField]
    private GameObject _clockWiseEnemyModel, _antiClockWiseEnemyModel;

    private CGB_EnemySpawner __enemySpawner;

    public void SetupEnemySpawnerReference(CGB_EnemySpawner spawner, ENEMY_TYPE type)
    {
        __enemySpawner = spawner;
        transform.tag = type.ToString();
        switch (type)
        {
            case ENEMY_TYPE.Clockwise:
                {
                    _clockWiseEnemyModel.SetActive(true);
                    _antiClockWiseEnemyModel.SetActive(false);
                    break;
                }
            case ENEMY_TYPE.AntiClockwise:
                {
                    _clockWiseEnemyModel.SetActive(false);
                    _antiClockWiseEnemyModel.SetActive(true);
                    break;
                }
        }
    }

    private void OnTriggerEnter(Collider hit)
    {
        if (hit.CompareTag("Player1") || hit.CompareTag("Player2"))
        {
            gameObject.SetActive(false);
            __enemySpawner.PoolEnemyCallback(this);
        }
    }
}
